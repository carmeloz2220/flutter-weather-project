import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:loader_overlay/loader_overlay.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.cyan,
      ),
      home: const LoaderOverlay(
          child: MyHomePage(title: 'Weather Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String country = '';
  String weather = '';
  String image = '';

  String searchText = '';

  var textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    country = '';
    weather = '';
    image = '';
  }

  void changeState(String _country, String _weather, String _image) {
    setState(() {
      country: _country;
      weather: _weather;
      image: _image;
    });
  }

  static const apiKey = "e4e62a370430437bb7d113519211711";

  Future getCurrentWeather() async {
    context.loaderOverlay.show();

    final url =
        Uri.parse("http://api.weatherapi.com/v1/current.json?key=$apiKey&q=$searchText");
    final response = await http.get(url);

    if (response.statusCode == 200) {
      context.loaderOverlay.hide();
    } else {
      context.loaderOverlay.hide();
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }

    final responseJson = json.decode(response.body);

    changeState(country = responseJson["location"]["name"],
        weather = responseJson["current"]["condition"]["text"],
        image = responseJson["current"]["condition"]["icon"]);

    return responseJson;
  }

  AlertDialog alert = const AlertDialog(
    title: Text("ERRORE!"),
    content: Text("Riprova con un altro paese")
  );

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        elevation: 0.5,
      ),
      body: Center(
        child: Card(
          child: Column(
            // Column is also a layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painti ng" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).

            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Flex(
                mainAxisAlignment: MainAxisAlignment.center,
                direction: Axis.horizontal,

                children: <Widget>[
                  Center(
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      decoration: const BoxDecoration(
                        color: Colors.cyan,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      width: 400.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(10),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          hintText: '  Write Country to discover the weather!',
                        ),
                        onFieldSubmitted: (String value) {
                          getCurrentWeather();
                        },
                        onChanged: (String value) {
                          setState(() { searchText = value; });
                        },
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      padding: const EdgeInsets.all(10),
                      height: 50.0,
                      child: ElevatedButton(
                        onPressed: getCurrentWeather,
                        child: const Icon(
                          Icons.search,
                          color: Colors.white,
                          size: 30.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Card(
                child: Column(
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 20.0),
                        child: Text(country, style: const TextStyle(fontSize: 50))
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                      child: (image != "" ? Image.network(image, width: 280.0, height: 200.0,) : null),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                      child: Text(
                        weather,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                  ],
                )
              )
            ],
          ),
        )
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
